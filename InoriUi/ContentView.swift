//
//  ContentView.swift
//  InoriUi
//
//  Created by piyawat kunama on 1/6/2565 BE.
//

import SwiftUI
import AFlib

//struct ContentView: View {
//   var body: some View {
//       AFlib.InPage(appKey: "na3g3y3fq", adKey: "8a22o0wv8")
//       AFlib.InPage(appKey: "fw0o1kyuy", adKey: "e0sd58g2x")
//
//   }
//}

struct ContentView: View {
    var body: some View {
        NavigationView {
            List(0..<100) { i in
                if(i==2){
                    AFlib.InPage(appKey: "hb85x7uc8", adKey: "ph3vdswwd")
                }
                Text("Swift UI Row \(i)").font(.system(size: 32))

            }
            .navigationTitle("Select a row")
            .safeAreaInset(edge: .bottom, alignment: .center, spacing: 0) {
                          Color.clear
                              .frame(height: 20)
                              .background(Material.bar)
                      }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}


